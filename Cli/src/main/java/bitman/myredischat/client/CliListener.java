package bitman.myredischat.client;

import bitman.myredischat.ChatListener;


public class CliListener
        extends ChatListener {


    public void onMessage(String channel, String message) {
        log(message);
    }

    @Override
    public int numMessages() {
        return 0;
    }

    @Override
    public void log(String msg) {
        System.out.println(msg);
    }
}
