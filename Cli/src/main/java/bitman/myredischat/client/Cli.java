package bitman.myredischat.client;

import bitman.myredischat.Chat;
import bitman.myredischat.ChatListener;
import bitman.myredischat.redis.RedisConstants;
import bitman.myredischat.redis.RedisDatabase;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Cli
{
    public static void main(String[] args)
            throws Throwable
    {
        ChatListener subscriber = new CliListener();
        String fromUsername = args[0];
        String toUsername = args[1];
        final Chat chat = new Chat( fromUsername, null, toUsername, subscriber,
                                    new RedisDatabase(RedisConstants.REDIS_HOSTNAME)
                                  );
        chat.connect();
        String line;
        final BufferedReader kbd = new BufferedReader(new InputStreamReader(System.in));
        while ((line = kbd.readLine()) != null)
        {
            if (line.equals("quit"))
            {
                chat.disconnect();
                System.exit(0);
            }

            chat.sendMessage(line);

        }
    }
}
