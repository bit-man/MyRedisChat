package bitman.myredischat.redis;

import redis.clients.jedis.JedisPool;

public class PublishComand
        implements RedisCommand<Long>
{
    private JedisPool pool;
    private PublishCommandData commandData;

    public PublishComand(JedisPool pool, PublishCommandData commandData)
    {
        this.pool = pool;
        this.commandData = commandData;
    }


    @Override
    public Long execute()
    {
        long[] ret = new long[1];
        RedisUtils.jedisRun(pool, j -> ret[0]  = j.publish(commandData.getChannelName(), commandData.getMessage()));

        return ret[0];
    }
}
