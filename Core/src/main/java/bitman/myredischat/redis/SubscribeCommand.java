package bitman.myredischat.redis;

import redis.clients.jedis.JedisPool;

public class SubscribeCommand
implements RedisCommand<Void>
{
    private final JedisPool pool;
    private SubscribeCommandData redisData;

    public SubscribeCommand(JedisPool pool, SubscribeCommandData redisData)
    {
        this.pool = pool;
        this.redisData = redisData;
    }

    @Override
    public Void execute()
    {
        RedisUtils.jedisRun(pool, j -> j.subscribe(redisData.getListener(), redisData.getChannelName()) );
        // FIXME avoid returning null
        return null;
    }


}
