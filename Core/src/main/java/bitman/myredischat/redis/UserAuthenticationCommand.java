package bitman.myredischat.redis;

import bitman.myredischat.Convert;
import redis.clients.jedis.JedisPool;

import java.security.NoSuchAlgorithmException;

public class UserAuthenticationCommand
        implements RedisCommand<Boolean>
{
    private JedisPool pool;
    private UserAuthenticationData data;

    public UserAuthenticationCommand(JedisPool pool, UserAuthenticationData data)
    {
        this.pool = pool;
        this.data = data;
    }

    @Override
    public Boolean execute()
    {
        String[] passMD5 = new String[1];
        RedisUtils.jedisRun(pool, j -> passMD5[0] = j.hget(Key.USER.get(data.getUserName()), RedisConstants.PASS_MD5));

        try
        {
            return Convert.toString(Convert.toMD5(data.getPassword())).equals(passMD5[0]);

        } catch (NoSuchAlgorithmException e)
        {
            return false;
        }

    }
}
