package bitman.myredischat.redis;

public enum Key
{
    USER("user:");

    private final String prefix;

    Key(String prefix)
    {
        this.prefix = prefix;
    }

    public String get(String s)
    {
        return prefix + s;
    }
}
