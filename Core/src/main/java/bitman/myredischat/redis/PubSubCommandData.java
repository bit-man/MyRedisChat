package bitman.myredischat.redis;

public class PubSubCommandData
{
    private final String channelName;

    public PubSubCommandData(String channelName)
    {
        this.channelName = channelName;
    }

    public String getChannelName()
    {
        return channelName;
    }

}
