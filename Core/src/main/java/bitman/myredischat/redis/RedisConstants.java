package bitman.myredischat.redis;


public class RedisConstants
{
    public static final String NO_PASSWORD = "";
    public static final String PASSWORD = System.getProperty("MyRedisChat.jedis.password", NO_PASSWORD);
    public static final String PASS_MD5 = "passMD5";

    private static final String DEFAULT_REDIS_HOSTNAME = "localhost";
    public static final String REDIS_HOSTNAME = System.getProperty("MyRedisChat.jedis.hostname", DEFAULT_REDIS_HOSTNAME);

}
