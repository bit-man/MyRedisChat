package bitman.myredischat.redis;

public interface RedisCommand<T>
{
    T execute();
}
