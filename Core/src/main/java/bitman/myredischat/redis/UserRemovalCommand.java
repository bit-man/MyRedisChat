package bitman.myredischat.redis;

import redis.clients.jedis.JedisPool;

public class UserRemovalCommand
    implements RedisCommand<Boolean>
{
    private final JedisPool pool;
    private final UserRemovalData userRemovalData;

    public UserRemovalCommand(JedisPool pool, UserRemovalData userRemovalData)
    {
        this.pool = pool;
        this.userRemovalData = userRemovalData;
    }

    @Override
    public Boolean execute()
    {
        final Boolean[] removed = {false};
        RedisUtils.jedisRun(pool, jedis -> {
                    String user = Key.USER.get(userRemovalData.getUserName());
                    if (!jedis.exists(user))
                    {
                        removed[0] = false;
                    } else
                    {
                        removed[0] = jedis.del(user) == 1;
                    }
                }

        );
        return removed[0];
    }
}
