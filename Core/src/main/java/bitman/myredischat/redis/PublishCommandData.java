package bitman.myredischat.redis;

public class PublishCommandData
{
    private final String channelName;
    private final String message;

    public PublishCommandData(String channelName, String message)
    {
        this.channelName = channelName;
        this.message = message;
    }

    public String getChannelName()
    {
        return channelName;
    }

    public String getMessage()
    {
        return message;
    }
}
