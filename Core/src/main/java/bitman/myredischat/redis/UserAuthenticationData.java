package bitman.myredischat.redis;

public class UserAuthenticationData
{
    private String userName;
    private String password;

    public UserAuthenticationData(String userName, String password)
    {
        this.userName = userName;
        this.password = password;
    }

    public String getUserName()
    {
        return userName;
    }

    public String getPassword()
    {
        return password;
    }
}
