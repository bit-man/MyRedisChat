package bitman.myredischat.redis;

import bitman.myredischat.Channel;
import bitman.myredischat.ChatAuthenticationException;
import bitman.myredischat.ChatListener;
import bitman.myredischat.Database;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisPubSub;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.List;

public class RedisDatabase
implements Database
{
    private final String redisHostname;
    private JedisPool pool;

    private Thread subscriberThread;
    private RedisSubscriber subscriber;

    public RedisDatabase(String redisHostname) {
        this.redisHostname = RedisConstants.REDIS_HOSTNAME;
    }

    public void publish(String message, Channel channel, ChatListener listener) {
        // Avoids race condition between subscriber and publisher (publisher sends message and subscriber
        // not subscribed yet)

        // ToDo PubSub and Publish commands obtain a new Jedis from pool and close it once each is executed
        // Change it to make them use the same Jedis and close it once last command ends.

        RedisCommand<List<String>> subscribedChannelsCmd = new PubSubChannelsCommand(pool, new PubSubCommandData(channel.getSendChannelName()));
        while(subscribedChannelsCmd.execute().isEmpty()) {
            listener.log("Waiting for user '" + channel.getToUsername() + "' to join chat");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

        new PublishComand(pool, new PublishCommandData(channel.getSendChannelName(), message)).execute();
    }

    public void connect() {
        if (pool == null) {
            pool = new JedisPool(new JedisPoolConfig(), redisHostname);
        }
    }

    @Override
    public void disconnect(ChatListener listener) {
        unsubscribe(listener);
        destroyRedisConnectionPool();
    }

    @Override
    public Boolean createUser(String userName, String password)
    {
        return new UserCreationCommand(pool, new UserCreationData(userName, password)).execute();
    }

    @Override
    public Boolean removeUser(String userName)
    {
        return new UserRemovalCommand(pool, new UserRemovalData(userName)).execute();
    }

    @Override
    public void chatAuthentication(String userName, String password)
            throws ChatAuthenticationException
    {
        if ( ! new UserAuthenticationCommand(pool, new UserAuthenticationData(userName, password)).execute() ) {
            throw new ChatAuthenticationException();
        }
    }

    private void destroyRedisConnectionPool() {
        if ( pool != null) {
            pool.destroy();
        }
    }

    private void unsubscribe(ChatListener listener) {
        if (subscriber != null) {
            subscriber.unsubscribe();

            if ( subscriberThread != null) {
                waitForSubscriberThreadFinish(listener);
            }
        }
    }

    private void waitForSubscriberThreadFinish(ChatListener listener) {
        try {
            subscriberThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace(new PrintWriter(new ListenerWriter(listener)));
        }
    }

    public void subscribe(JedisPubSub listener, Channel channel) {
        subscriber = new RedisSubscriber(listener, channel);
        subscriberThread = new Thread(subscriber);
        subscriberThread.start();
    }

    private class RedisSubscriber
            implements Runnable {

        private final JedisPubSub listener;
        private final Channel channel;

        public RedisSubscriber(JedisPubSub listener, Channel channel)
        {
            this.listener = listener;
            this.channel = channel;
        }

        @Override
        public void run() {
            // Jedis subscribe is a blocking operation thus implemented inside a subscriberThread
            new SubscribeCommand( pool, new SubscribeCommandData(listener, channel.getReceiveChannelName()) ).execute();
        }

        public void unsubscribe() {
            listener.unsubscribe(channel.getReceiveChannelName());
        }
    }

    private class ListenerWriter
            extends Writer {
        private final ChatListener listener;

        public ListenerWriter(ChatListener listener) {
            this.listener = listener;
        }

        @Override
        public void write(char[] cbuf, int off, int len)
                throws IOException {
            listener.log(new String(cbuf));
        }

        @Override
        public void flush()
                throws IOException {

        }

        @Override
        public void close()
                throws IOException {

        }
    }
}
