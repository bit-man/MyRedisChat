package bitman.myredischat.redis;

import redis.clients.jedis.JedisPubSub;

public class SubscribeCommandData
{


    private final JedisPubSub listener;
    private final String channelName;

    public SubscribeCommandData(JedisPubSub listener, String channelName)
    {
        this.listener = listener;
        this.channelName = channelName;
    }

    public JedisPubSub getListener()
    {
        return listener;
    }

    public String getChannelName()
    {
        return channelName;
    }

}
