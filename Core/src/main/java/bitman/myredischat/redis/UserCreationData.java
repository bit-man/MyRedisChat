package bitman.myredischat.redis;

public class UserCreationData
{
    private final String userName;
    private String password;

    public UserCreationData(String userName, String password)
    {
        this.userName = userName;
        this.password = password;
    }

    public String getUserName()
    {
        return userName;
    }

    public String getPassowrd()
    {
        return password;
    }
}
