package bitman.myredischat.redis;


public class UserRemovalData
{
    private final String userName;

    public UserRemovalData(String userName)
    {
        this.userName = userName;
    }

    public String getUserName()
    {
        return userName;
    }

}
