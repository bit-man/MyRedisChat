package bitman.myredischat.redis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.function.Consumer;

public class RedisUtils
{
    public static void jedisRun(JedisPool pool, Consumer<Jedis> function) {
        try (Jedis jedis = pool.getResource()) {
            if (! RedisConstants.PASSWORD.equals(RedisConstants.NO_PASSWORD)) {
                jedis.auth(RedisConstants.PASSWORD);
            }

            function.accept(jedis);
        }
    }
}
