package bitman.myredischat.redis;

import redis.clients.jedis.JedisPool;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PubSubChannelsCommand
    implements RedisCommand<List<String>>
{
    private JedisPool pool;
    private PubSubCommandData commandData;

    public PubSubChannelsCommand(JedisPool pool, PubSubCommandData commandData)
    {
        this.pool = pool;
        this.commandData = commandData;
    }


    @Override
    public List<String> execute()
    {
        final String[][] data = new String[1][];
        RedisUtils.jedisRun(pool, j -> data[0] = j.pubsubChannels(commandData.getChannelName())
                .toArray(new String[0])
        );

        List<String> retData = new ArrayList<>(data[0].length);
        Collections.addAll(retData, data[0]);
        return retData;
    }
}
