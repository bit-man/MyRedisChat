package bitman.myredischat.redis;

import bitman.myredischat.ChatRuntimeException;
import bitman.myredischat.Convert;
import redis.clients.jedis.JedisPool;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

public class UserCreationCommand
        implements RedisCommand<Boolean>
{

    private static final String LAST_LOGIN = "lastLogin";
    private static final String LAST_ACTIVITY = "lastActivity";

    private final JedisPool pool;
    private final UserCreationData data;

    public UserCreationCommand(JedisPool pool, UserCreationData data)
    {
        this.pool = pool;
        this.data = data;
    }

    @Override
    public Boolean execute()
    {
        final Boolean[] userCreated = {false};
        RedisUtils.jedisRun(pool, jedis -> {
                    String user = Key.USER.get(data.getUserName());
                    if (jedis.exists(user))
                    {
                        userCreated[0] = false;
                    } else
                    {
                        try
                        {
                            userCreated[0] = jedis.hmset(user, createUserStructure()).equals("OK");
                        } catch (ChatRuntimeException e)
                        {
                            userCreated[0] = false;
                        }
                    }
                }

        );
        return userCreated[0];
    }

    private HashMap<String, String> createUserStructure()
    {
        HashMap<String, String> map = new HashMap<>();
        map.put(LAST_LOGIN, String.valueOf(System.currentTimeMillis()));
        map.put(LAST_ACTIVITY, String.valueOf(System.currentTimeMillis()));

        byte[] passwordHash;
        try
        {
            passwordHash = Convert.toMD5(data.getPassowrd());

        } catch (NoSuchAlgorithmException e)
        {
            throw new ChatRuntimeException(e);
        }

        map.put(RedisConstants.PASS_MD5, Convert.toString(passwordHash));

        return map;
    }

}
