package bitman.myredischat;

import redis.clients.jedis.JedisPubSub;

public interface Database {

    /**
     * Message publishing to outgoing channel
     * @param message Message to send
     * @param channel channel
     * @param listener listener for message reciving
     */
    void publish(String message, Channel channel, ChatListener listener);

    /**
     * Incoming channel subscription
     * @param listener listener for message reciving
     * @param channel channel
     */
    void subscribe(JedisPubSub listener, Channel channel);


    /**
     * Database connection.
     * Use it as first command before chat starts.
     */
    void connect();

    /**
     * Database disconnection.
     * Use it before your program exit.
     * @param listener channel
     */
    void disconnect(ChatListener listener);

    Boolean createUser(String userName, String password);

    Boolean removeUser(String userName);

    void chatAuthentication(String userName, String password)
            throws ChatAuthenticationException;
}
