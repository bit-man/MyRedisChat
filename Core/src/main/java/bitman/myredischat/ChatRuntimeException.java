package bitman.myredischat;

public class ChatRuntimeException
        extends RuntimeException
{
    public ChatRuntimeException(Throwable e)
    {
        super(e);
    }
}
