package bitman.myredischat;

public class Channel {
    private String fromUsername;
    private String toUsername;

    public Channel(String fromUsername, String toUsername) {
        this.fromUsername = fromUsername;
        this.toUsername = toUsername;
    }

    public String getSendChannelName() {
        return fromUsername + "->" + toUsername;
    }

    public String getReceiveChannelName() {
        return toUsername + "->" + fromUsername;
    }

    public String getToUsername() {
        return toUsername;
    }
}
