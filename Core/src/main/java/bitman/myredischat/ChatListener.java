package bitman.myredischat;

import redis.clients.jedis.JedisPubSub;

public abstract class ChatListener
        extends JedisPubSub {
    public abstract int numMessages();

    public abstract void log(String msg);
}
