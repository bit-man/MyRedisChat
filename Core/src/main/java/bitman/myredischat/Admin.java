package bitman.myredischat;

public class Admin
    extends DatabaseCommand
{

    public Admin(Database db, ChatListener listener)
    {
        super(db, listener);
    }

    public Boolean createUser(String name, String password){
        return db.createUser(name, password);
    }

    public Boolean removeUser(String name){
        return db.removeUser(name);
    }
}
