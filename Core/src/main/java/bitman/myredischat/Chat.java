package bitman.myredischat;

public class Chat
        extends DatabaseCommand
{

    private final Channel channel;
    private final String password;
    private final String username;

    public Chat(String fromUsername, String fromPassword, String toUsername, ChatListener listener, Database db)
    {
        super(db, listener);
        this.channel = new Channel(fromUsername, toUsername);
        this.username = fromUsername;
        this.password = fromPassword;
    }

    public void connect()
            throws ChatAuthenticationException
    {
        super.connect();
        authenticate(username,password);
        db.subscribe(listener, channel);
    }

    public void sendMessage(String message)
    {
        db.publish(message, channel, listener);
    }

}
