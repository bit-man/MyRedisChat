package bitman.myredischat;

public abstract class DatabaseCommand
{
    protected final Database db;
    protected final ChatListener listener;

    public DatabaseCommand(Database db, ChatListener listener) {
        this.db = db;
        this.listener = listener;
    }
    public void connect()
            throws ChatAuthenticationException
    {
        db.connect();
    }

    public void disconnect() {
        db.disconnect(listener);
    }

    protected void authenticate(String userName, String password)
            throws ChatAuthenticationException
    {
        db.chatAuthentication(userName,password);
    }
}
