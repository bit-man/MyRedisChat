package bitman.myredischat;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class Convert
{
    public static String toString(byte[] bytes)
    {
        return Base64.getEncoder().encodeToString(bytes);
    }

    public static byte[] toMD5(String s)
            throws NoSuchAlgorithmException
    {
        return MessageDigest.getInstance("MD5").digest(s.getBytes());
    }
}
