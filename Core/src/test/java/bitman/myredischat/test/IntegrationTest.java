package bitman.myredischat.test;


import com.github.ferstl.junit.testgroups.TestGroup;
import com.github.ferstl.junit.testgroups.TestGroupRule;
import org.junit.ClassRule;

@TestGroup("integration")
public abstract class IntegrationTest
{
    @ClassRule
    public static TestGroupRule rule = new TestGroupRule();

}
