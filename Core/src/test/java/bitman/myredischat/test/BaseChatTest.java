package bitman.myredischat.test;

import bitman.myredischat.Chat;
import bitman.myredischat.ChatAuthenticationException;
import bitman.myredischat.Database;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.testcontainers.containers.GenericContainer;

import static bitman.myredischat.test.TestUtils.*;
import static org.junit.Assert.assertEquals;

public abstract class BaseChatTest
{
    @Rule
    public GenericContainer redis = new GenericContainer("redis:5.0.3-alpine")
            .withExposedPorts(6379);

    @Before
    public void before()
    {
        prepareDatabase(getDatabase(redis.getHost()));
    }

    @Test
    public void firstBasicChat()
            throws ChatAuthenticationException
    {
        final MyTestListener user2Listener = new MyTestListener();
        final Chat user2chat = new Chat(USER_2, PASSWORD, USER_1, user2Listener,
                getDatabase(redis.getHost()));

        final MyTestListener user1Listener = new MyTestListener();
        final Chat user1chat = new Chat(USER_1, PASSWORD, USER_2, user1Listener,
                getDatabase(redis.getHost())
        );

        basicChatConnectSendAndAssert(user2Listener, user2chat, user1Listener, user1chat);

    }


    private void basicChatConnectSendAndAssert(MyTestListener user2Listener, Chat user2chat,
                                               MyTestListener user1Listener,
                                               Chat user1chat)
            throws ChatAuthenticationException
    {
        user2chat.connect();
        user1chat.connect();

        assertEquals(0, user2Listener.numMessages());
        assertEquals(0, user1Listener.numMessages());

        user1chat.sendMessage("Hello, user2!");

        user2Listener.assertNumMessagesChangedTo(1);
        assertEquals(0, user1Listener.numMessages());

        user2chat.sendMessage("Hello, back!");

        assertEquals(1, user2Listener.numMessages());
        user1Listener.assertNumMessagesChangedTo(1);
    }

    @Test(expected = ChatAuthenticationException.class)
    public void testChatWithInexistentUserFails()
            throws ChatAuthenticationException
    {

        final MyTestListener listener = new MyTestListener();

        new Chat(INEXISTENT_USER, PASSWORD, USER_1, listener, getDatabase(redis.getHost())).connect();

    }

    @Test(expected = ChatAuthenticationException.class)
    public void testChatWithUser1AndWrongPasswordFails()
            throws ChatAuthenticationException
    {

        final MyTestListener listener = new MyTestListener();

        new Chat(USER_1, WRONG_PASSWORD, USER_2, listener, getDatabase(redis.getHost())).connect();

    }

    public void testChatWithUser1AndRightPasswordEndsOK()
            throws ChatAuthenticationException
    {

        final MyTestListener listener = new MyTestListener();

        new Chat(USER_1, PASSWORD, USER_2, listener, getDatabase(redis.getHost())).connect();

    }

    protected abstract Database getDatabase(String hostname);
}
