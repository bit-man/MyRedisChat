package bitman.myredischat.test;

import bitman.myredischat.Channel;
import bitman.myredischat.ChatAuthenticationException;
import bitman.myredischat.ChatListener;
import bitman.myredischat.Database;
import redis.clients.jedis.JedisPubSub;

import java.util.HashMap;
import java.util.Map;

public class MockedDatabase
        implements Database {

    private Map<String, String> users = new HashMap<>();
    private Map<String, JedisPubSub> channels = new HashMap<>();

    public MockedDatabase() {
    }

    @Override
    public void publish(String message, Channel channel, ChatListener listener) {
        channels.get(channel.getSendChannelName()).onMessage(channel.getSendChannelName(), message);
    }

    @Override
    public void connect() {

    }

    @Override
    public void disconnect(ChatListener listener) {
        for( String channelName : channels.keySet() ) {
            if ( channels.get(channelName) == listener ) {
                channels.remove(channelName);
                return;
            }
        }
    }

    @Override
    public Boolean createUser(String userName, String password)
    {
        users.put(userName, password);
        return true;
    }

    @Override
    public Boolean removeUser(String userName)
    {
        users.remove(userName);
        return true;
    }

    @Override
    public void chatAuthentication(String userName, String password)
            throws ChatAuthenticationException
    {
        if ( ! users.containsKey(userName) ||  ! users.get(userName).equals(password) ) {
            throw new ChatAuthenticationException();
        }
    }

    @Override
    public void subscribe(JedisPubSub listener, Channel channel) {
        channels.put(channel.getReceiveChannelName(), listener);
    }
}
