package bitman.myredischat.test;

import bitman.myredischat.Admin;
import bitman.myredischat.ChatAuthenticationException;
import bitman.myredischat.redis.RedisConstants;
import bitman.myredischat.redis.RedisDatabase;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AdminIntegrationTest
    extends IntegrationTest
{

    private static final RedisDatabase db = new RedisDatabase(RedisConstants.REDIS_HOSTNAME);

    @Before
    public void  before() {
        TestUtils.prepareDatabase(db);
    }

    @Test
    public void testCreateInexistentUserEndsOK()
            throws ChatAuthenticationException
    {
        Admin admin = new Admin(db, new MyTestListener());
        admin.connect();
        assertTrue( admin.createUser(TestUtils.INEXISTENT_USER, "password") );
    }

    @Test
    public void testDeleteExistentUserEndsOK()
            throws ChatAuthenticationException
    {
        Admin admin = new Admin(db, new MyTestListener());
        admin.connect();
        assertTrue( admin.removeUser(TestUtils.UNDELETABLE_USER) );
    }

    @Test
    public void testDeleteInexistentUserFails()
            throws ChatAuthenticationException
    {
        Admin admin = new Admin(db, new MyTestListener());
        admin.connect();
        assertFalse( admin.removeUser(TestUtils.INEXISTENT_USER) );
    }
}
