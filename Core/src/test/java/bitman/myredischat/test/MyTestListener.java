package bitman.myredischat.test;

import bitman.myredischat.ChatListener;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import static org.junit.Assert.assertEquals;

public class MyTestListener
        extends ChatListener
{

    private BlockingQueue<String> queue = new ArrayBlockingQueue<>(10);
    private boolean hasChanged = false;
    private boolean silent = true;

    public void onMessage(String channel, String message)
    {
        log("New message : " + message);
        setChangeMark("onMessage");
        this.queue.add(message);
    }

    public int numMessages()
    {
        clearChangeMark("numMessages");
        return queue.size();
    }

    public synchronized void assertNumMessagesChangedTo(int expectedNumMessages)
    {
        waitForChange("assertNumMessagesChangedTo");
        clearChangeMark("assert");
        assertEquals(expectedNumMessages, queue.size());
    }

    private synchronized void clearChangeMark(String msg)
    {
        log(msg + " - START");
        this.hasChanged = false;
        log(msg + " - POST NOTIFY");
    }

    private synchronized void setChangeMark(String msg)
    {
        log("setChangeMark (START) : " + msg);
        this.hasChanged = true;
        notifyAll();
        log("setChangeMark (POST NOTIFY) : " + msg);
    }

    private synchronized void waitForChange(String msg)
    {
        log("Wait4change (START) : " + msg);
        if (hasChanged)
        {
            clearChangeMark(null);
            log("Wait4change (RETURN) : " + msg);
            return;
        }

        log("Wait4change (! hasChanged) : " + msg);

        try
        {
            while (!hasChanged)
            {
                log("Wait4change (wait) : " + msg);
                wait();
            }
        } catch (InterruptedException e)
        {
            throw new RuntimeException(e);
        }
        log("Wait4change (EXIT) : " + msg);
    }

    public void log(String msg)
    {
        if (!this.silent)
        {
            System.out.println(msg);
        }
    }
}
