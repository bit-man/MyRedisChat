package bitman.myredischat.test;

import bitman.myredischat.Database;
import bitman.myredischat.redis.RedisDatabase;

public class ChatIntegrationTest
    extends BaseChatTest {

    @Override
    protected Database getDatabase(String hostname)
    {
        return new RedisDatabase(hostname);
    }

}
