package bitman.myredischat.test;

import bitman.myredischat.Database;

public class TestUtils
{
    public static final String INEXISTENT_USER = "inexistentUser";
    public static final String UNDELETABLE_USER = "TheUndeletable";
    public static final String USER_1 = "User1";
    public static final String USER_2 = "User2";
    public static final String PASSWORD = "password";
    public static final String WRONG_PASSWORD = "wrongPassword";

    public static void prepareDatabase(Database db)
    {
        db.connect();
        db.removeUser(INEXISTENT_USER);
        db.removeUser(UNDELETABLE_USER);
        db.removeUser(USER_1);
        db.removeUser(USER_2);
        db.createUser(UNDELETABLE_USER, PASSWORD);
        db.createUser(USER_1, PASSWORD);
        db.createUser(USER_2, PASSWORD);
    }
}
