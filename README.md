MyRedisChat
===========

A simple chat used as testbed for Redis learning.

Implementation
--------------

The basic building blocks are Redis channels and publish/subscribe mechanism.
On each user pair that initiate chat one channel is created for every user as depicted 

![Channels](MyRedisChat-Channels.png)

Setup
-----

  * Install and run a Redis instance
  * [Install Java 8](https://github.com/hgomez/obuildfactory/wiki/How-to-build-and-package-OpenJDK-8-on-Linux)
  * Install GIT and Maven
  * Clone this git repository
  * Compile MyRedisChat from this project root folder

Build command :

     mvn clean package -DMyRedisChat.jedis.password=MyRedisPassword -Dtestgroup=all
     
In case you want to avoid test execution just type

     mvn clean package -DskipTests

To avoid running resource dependent tests (needs Redis database running)

     mvn clean package
     
Integration tests execution only :

     mvn clean package -DMyRedisChat.jedis.password=MyRedisPassword -Dtestgroup=integration
     
 If you Redis install does not need authentication avoid setting the property *MyRedisChat.jedis.password*
     
CLI client
----------
     
To run client one instance must be run for each user willing to communicate (Homer and Marge), being the next
command line for Homer computer 

    java -DMyRedisChat.jedis.password=MyRedisPassword -cp Core/target/Core-1.0-SNAPSHOT.jar:Cli/target/Cli-1.0-SNAPSHOT.jar:/path/to/jedis-2.7.2.jar:/path/to/commons-pool2-2.3.jar bitman.myredischat.client.Cli Homer Marge
 
and the next one for Marge

    java -DMyRedisChat.jedis.password=MyRedisPassword -cp Core/target/Core-1.0-SNAPSHOT.jar:Cli/target/Cli-1.0-SNAPSHOT.jar:/path/to/jedis-2.7.2.jar:/path/to/commons-pool2-2.3.jar bitman.myredischat.client.Cli Marge Homer
    
    
Both can start typing messages that will be shown in each other screen. To exit MyRedisChat just type **quit**

Enjoy !